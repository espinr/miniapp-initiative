# Quick App Long Jump Competition 

Sample code of a quick app to show real-time results in a long jump athletics competition.

This example was presented during the [AthTech'21 Conference](https://athtech.run/2021) as a proof of concept to easily visualize sports data in real-time. 

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./longjump.gif" alt="Quick App execution" /> 