# QAI Launch Participant: **Alliance Tech**

<!-- LOGO HERE -->
[![Alliance Tech logo](https://alliance-tech.org/wp-content/uploads/2020/05/Alliance-Tech_logo_silvergreen.png)](https://alliance-tech.org/)

European Alliance for Technology Development (Alliance Tech) is Association that offers a co-creation framework for investors, entrepreneurs, private and public sector leaders, academia and R&D centers, developing uses cases.

<!-- additional text here -->


<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->


<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organization information -->
#
Country of incorporation/registration: Paris, France

Type of organization: non-profit <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: <!-- short list/keywords here -->

Website: https://alliance-tech.org/ <!-- website URL; normally the same as the logo click target -->

QAI representatives:
* Adrien Henni
* Konstantine Karczmarski

<!-- any other contact information that you would like to show -->
