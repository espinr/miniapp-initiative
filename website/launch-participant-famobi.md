# QAI Launch Participant: **Famobi**

<!-- LOGO HERE -->
[![Famobi logo](https://ik.imagekit.io/sjyfpsp1n/fa/assets/img/FbShareImage.jpg)](https://famobi.com/?locale=en )

Pioneer in the H5 gaming industry, using the newest cross-platform technology to bring native app-quality to the web. Famobi has been working with Quick Apps since 2019.

<!-- additional text here -->


<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->


<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organization information -->
#
Country of incorporation/registration: Cologne, Germany

Type of organization: Company <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: Mobile games development <!-- short list/keywords here -->

Website: https://famobi.com/?locale=en <!-- website URL; normally the same as the logo click target -->

QAI representative: Ilker Aydin

<!-- any other contact information that you would like to show -->
