# Audio Volume

__System's multimedia volume control.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.volume"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import volume from '@system.volume' 
```

Or

``` js
let volume = require("@system.volume")
```


## Methods

This service has the following methods:

- [`getMediaValue({success,fail,complete})`](#getmediavalue-success-fail-complete)
- [`setMediaValue({value,success,fail,complete})`](#setmediavalue-value-success-fail-complete)

### `getMediaValue({success,fail,complete})`

__This method gets the current multi-media volume__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(object)`). Optional callback function for success. The function has an `object` argument with the following attributes:
  - `value` (`number`). Percentage of the volume. The value is a decimal number from `0.0` to `1.0`.
- `fail` (`function()`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
volume.getMediaValue({
    success: function (ret) {
        console.log("Value:" + ret.value * 100)
    }
})
```

### `setMediaValue({value,success,fail,complete})`

__This method sets the multi-media volume of the system__.

#### Arguments

This method requires an `object` with the following attributes:
- `value` (`number`). Mandatory attribute with the percentage of the volume as a decimal number from `0.0` to `1.0`.
- `success`	(`function()`). Optional callback function for success. 
- `fail` (`function(data,code)`). Optional callback function for failure. This method can generate the following codes:
  - `200`: Common error.
  - `202`: Invalid parameter.  
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
volume.setMediaValue({
    volume: 1.0,
    success: function (data) {
    },
    fail: function (data, code) {
        prompt.showToast({
            message: 'fail',
            duration: 2000,
            gravity: 'bottom'
        })
    }
})
```