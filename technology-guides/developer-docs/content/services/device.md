# Device Information

__Information about the device.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.device"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import device from '@system.device' 
```

Or

``` js
let device = require("@system.device")
```


## Attributes


This service has the following attributes:

- [`platform`](#platform)
- [`allowTrackOAID`](#allowtrackoaid)

### `platform`

__Platform information.__ 

- Type: `object` with the following member:
  - `versionCode` indicating the platform version
- Mandatory: no
- Value by default: -

### `allowTrackOAID`

__Indicates whether OAIDs and device IDs can be used for tracing (on Android 10+).__ 

- Type: `boolean` 
- Mandatory: no
- Value by default: -

Value `true` indicates that these identifiers may be used for device tracing.

## Methods

This service has the following methods:

- [`getInfo({success,fail,complete})`](#getinfo-success-fail-complete)
- [`getInfoSync()`](#getinfosync)
- [`getId({type,success,fail,complete})`](#getid-type-success-fail-complete)
- [`getDeviceId({success,fail,complete})`](#getdeviceid-success-fail-complete)
- [`getUserId({success,fail,complete})`](#getuserid-success-fail-complete)
- [`getOAID({success,fail,complete})`](#getoaid-success-fail-complete)
- [`getTotalStorage({success,fail,complete})`](#gettotalstorage-success-fail-complete)
- [`getSerial({success,fail,complete})`](#getserial-success-fail-complete)
- [`getAvailableStorage({success,fail,complete})`](#getavailablestorage-success-fail-complete)
- [`getCpuInfo({success,fail,complete})`](#getcpuinfo-success-fail-complete)
- [`getTheme({success,fail,complete})`](#gettheme-success-fail-complete)
- [`getThemeSync()`](#getthemesync)
- [`getServiceCountryCode({success,fail,complete})`](#getservicecountrycode-success-fail-complete)
- [`toLocaleDateTimeString(timestamp)`](#tolocaledatetimestring-timestamp)
- [`toLocaleDateString(timestamp)`](#tolocaledatestring-timestamp)
- [`toLocaleTimeString(timestamp)`](#tolocaletimestring-timestamp)
- [`host({success,fail,complete})`](#host-success-fail-complete)

### `getInfo({success,fail,complete})`

__Obtains device information.__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the following members:
  - `brand` (`string`). Device brand.
  - `manufacturer` (`string`). Device manufacturer.
  - `model` (`string`). Device model.
  - `product` (`string`). Device code.
  - `osType` (`string`). Operating system name.
  - `osVersionName` (`string`). Operating system version name.
  - `osVersionCode` (`number`). Operating system version code.
  - `platformVersionName` (`string`). Platform version name.
  - `platformVersionCode` (`number`). Platform version code.
  - `language` (`string`). System language.
  - `region` (`string`). System country/region.
  - `screenWidth` (`number`). Screen width.
  - `screenHeight` (`number`). Screen height.
  - `engineProvider` (`string`). Provider of the quick app engine. 
  - `windowWidth` (`number`). Available window width, which is determined by the actual screen resolution.
  - `windowHeight` (`number`). Available window height, which is determined by the actual screen resolution. (Available window height = Screen height - Status bar height - Title bar height - Virtual navigation bar height)
  - `statusBarHeight` (`number`). Status bar height, which is determined by the actual screen resolution.
  - `pixelRatio` (`number`). Device pixel ratio.
  - `screenDensity` (`number`). Screen density of the device.
  - `timeZone` (`string`). Current system time zone.
  - `fontScale` (`number`). Font size.
  - `characteristics` (`string`). Device type (options are `default`, `tv`, `tablet`, and `phone`). The default value is `phone`.
  - `windowLogicWidth` (`number`). The base value for a widget is `750`, a normal quick app, the value of this parameter is the same as the value of [`designWidth`](../guide/manifest.html#designwidth) in the `config` member of the manifest. The default value is `750`.
  - `windowLogicHeight` (`number`). The base value for a widget is `750`.
  - `marketingName` (`string`). Model marketing name of the device.
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getInfo({ 
    success: function (res) { 
        console.log(JSON.stringify(res)); 
    }, 
    fail: function (erromsg, errocode) { 
        console.log("message:",erromsg,errocode); 
    } 
})
```

Returning:

``` bash
{"brand":"HONOR","manufacturer":"HUAWEI","model":"ATH-TL00H","product":"ATH-TL00H","osType":"android","osVersionName":"5.1.1","osVersionCode":22,"platformVersionName":"1.040","platformVersionCode":1040,"language":"en","region":"UK","screenWidth":1080,"screenHeight":1776,"timeZone":"GMT+08:00","emuiVer":"3.1","fontScale":1,"characteristics":"default","windowWidth":1080,"windowHeight":1557,"statusBarHeight":75,"pixelRatio":3,"engineProvider":"huawei","screenDensity":3,"screenLogicWidth":750,"screenLogicHeight":1233,"windowLogicWidth":750,"windowLogicHeight":1081}
```

### `getInfoSync()`

__Obtains device information__.

#### Return

This method returns an `object` with the following attributes:
- `brand` (`string`). Device brand.
- `manufacturer` (`string`). Device manufacturer.
- `model` (`string`). Device model.
- `product` (`string`). Device code.
- `osType` (`string`). Operating system name.
- `osVersionName` (`string`). Operating system version name.
- `osVersionCode` (`string`). Operating system version code.
- `platformVersionName` (`string`). Platform version name.
- `platformVersionCode` (`number`). Platform version code.
- `language` (`string`). System language.
- `region` (`string`). System country/region.
- `screenWidth` (`number`). Screen width.
- `screenHeight` (`number`). Screen height.
- `engineProvider` (`string`). Provider of the quick app engine.
- `windowWidth` (`number`). Available window width, which is determined by the actual screen resolution.
- `windowHeight` (`number`). Available window height, which is determined by the actual screen resolution. (Available window height = Screen height - Status bar height - Title bar height - Virtual navigation bar height)
- `statusBarHeight` (`number`). Status bar height, which is determined by the actual screen resolution.
- `pixelRatio` (`number`). Device pixel ratio.
- `screenDensity` (`number`). Screen density of the device.
- `timeZone` (`string`). Current system time zone.
- `fontScale` (`number`). Font size.
- `characteristics` (`string`). Device type (options are `default`, `tv`, `tablet`, and `phone`). The default value is `phone`.
- `windowLogicWidth` (`number`). The base value for a widget is `750`, a normal quick app, the value of this parameter is the same as the value of [`designWidth`](../guide/manifest.html#designwidth) in the `config` member of the manifest. The default value is `750`.
- `windowLogicHeight` (`number`). The base value for a widget is `750`.
- `marketingName` (`string`). Model marketing name of the device.


Example:

``` js 
try { 
    const res = device.getInfoSync(); 
    console.log(res.brand); 
    console.log(res.model); 
} catch (e) { 
    // Do something upon an error. 
    console.log(e.code + e.message); 
}
```


### `getId({type,success,fail,complete})`

__Obtains the device ID, user ID, and MAC address__.

#### Arguments

This method requires an `object` with the following attributes:
- `type` (`array`). Mandatory attribute with the types of identifier to be retrieved. The options are: `device`, `mac`, and `user`.
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the identifier(s) requested:
  - `device` (`string`). Unique device ID. In versions earlier than Android 10.0, the IMEI or MEID is returned. In Android 10.0 and later versions, the AAID is returned, which is unique to each quick app. 
  - `mac` (`string`). MAC address of a device. In versions earlier than Android 6.0, the MAC address of the device is returned. In Android 6.0 and later versions, a fixed value is returned.
  - `user` (`string`). Unique user ID. In versions earlier than Android 10.0, the value of `ANDROID_ID` is returned. In Android 10.0 and later versions, the value varies depending on the signing certificate fingerprint of the quick app.
- `fail` (`function(code)`). Optional callback function for failure. The code of this function can be:
  - `201` when the user has rejected the request for sharing the information.
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
device.getId({ 
    type: ["device", "mac"], 
    success:function(data) { 
        console.log("handling success: " + data.device); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getDeviceId({success,fail,complete})`

__Obtains the identifier of the device__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the identifier requested:
  - `deviceId` (`string`). Unique device ID. In versions earlier than Android 10.0, the IMEI or MEID is returned. In Android 10.0 and later versions, the AAID is returned, which is unique to each quick app. 
- `fail` (`function(code)`). Optional callback function for failure. The code of this function can be:
  - `201` when the user has rejected the request for sharing the information.
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
device.getDeviceId({ 
    success: function(data) { 
        console.log("handling success: " + data.deviceId); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getUserId({success,fail,complete})`

__Obtains the identifier of the user__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the identifier requested:
  - `userId` (`string`). Unique user ID. In versions earlier than Android 10.0, the value of `ANDROID_ID` is returned. In Android 10.0 and later versions, the value varies depending on the signing certificate fingerprint of the quick app.
- `fail` (`function(code)`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
device.getUserId({ 
    success: function(data) { 
        console.log("handling success: " + data.userId); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```


### `getOAID({success,fail,complete})`

__Retrieves the OAID of a device__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the identifier requested:
  - `oaid` (`string`). OAID of the device. If the current device does not support OAID, the system will return `null`. If the user configures the system to disable personalized ads on the device, the value will be a string with zeros.
- `fail` (`function(code)`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js
device.getOAID({ 
        success: function (ret) { 
            that.OAID = JSON.stringify(ret); 
        }, 
        fail: function (erromsg, errocode) { 
            that.OAID = errocode + ': ' + erromsg 
        }, 
        complete: function () { 
            prompt.showToast({ 
                message: "The request is completed." + device.allowTrackOAID 
            }) 
        } 
})
```

### `getTotalStorage({success,fail,complete})`

__This method gets the total internal storage space, excluding the storage space provided by an external SD card__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the value requested:
  - `totalStorage` (`number`). Total storage space, in bytes.
- `fail` (`function(code)`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getTotalStorage({ 
    success:function(data) { 
        console.log("handling success: " + data.totalStorage); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getSerial({success,fail,complete})`

__Gets the serial number of a device__.

The user must grant the permission to access this data.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the identifier requested:
  - `serial` (`string`). Serial number of the device.
- `fail` (`function(code)`). Optional callback function for failure. This function has an argument that will be `201` when the user rejects the authorization.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getSerial({ 
    success: function (ret) { 
        console.log("handling success: " +JSON.stringify(ret)); 
    }, 
    fail: function (erromsg, errocode) { 
        console.log("handling success: "+ errocode + ': ' + erromsg) 
    }, 
    complete: function () { 
        prompt.showToast({ 
            message: "The request is completed." 
        }) 
    } 
})
```

### `getAvailableStorage({success,fail,complete})`

__Gets the available internal storage space, excluding the external SD cards.__

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the following attribute:
  - `availableStorage` (`number`). Available storage space, in bytes.
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getAvailableStorage({ 
    success:function(data) { 
        console.log("handling success: " + data.availableStorage); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getCpuInfo({success,fail,complete})`

__Method to get information about the CPU of the device__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the following attribute:
  - `cpuInfo` (`string`). CPU information. For an Android device, the system returns the content of the `/proc/cpuinfo` file.
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getCpuInfo({ 
    success:function(data) { 
        console.log("handling success: " + data.cpuInfo); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `getTheme({success,fail,complete})`

__Gets information about the theme enabled in the system.__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the the following attributes:
  - `title` (`string`). Mandatory attribute with the name of the current theme.
  - `titleCn` (`string`). Optional attribute with the Chinese name of the current theme.
  - `author` (`string`). Mandatory attribute with the author of the current theme.
  - `designer` (`string`). Optional	attribute with the designer of the current theme.
  - `version` (`string`). Optional attribute with the version number of the current theme.
  - `font` (`string`). Optional attribute with the font name of the current theme.
  - `fontCn` (`string`). Optional attribute with the name of the Chinese font used in the current theme.
  - `darkMode` (`boolean`). Mandatory attribute that indicates whether the dark theme is used (`true`) or not.
- `fail` (`function(code)`). Optional callback function for failure. This function has an argument that may have the following values:
  - `200`: General error.
  - `203` : Service unavailable.
  - `301` : The theme file or dark theme information does not exist.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getTheme({ 
    success: function(ret) { 
        console.log("theme title" + ret.title); 
    } 
});
```

### `getThemeSync()`

__Gets information about the theme enabled in the system (synchronously).__.

#### Return

This method returns an `object` with the following attributes:
- `title` (`string`). Attribute with the name of the current theme.
- `titleCn` (`string`). Attribute with the Chinese name of the current theme.
- `author` (`string`). Attribute with the author of the current theme.
- `designer` (`string`). Attribute with the designer of the current theme.
- `version` (`string`). Attribute with the version number of the current theme.
- `font` (`string`). Attribute with the font name of the current theme.
- `fontCn` (`string`). Attribute with the name of the Chinese font used in the current theme.
- `darkMode` (`boolean`). Attribute that indicates whether the dark theme is used (`true`) or not.

In case of failure, the method returns the following codes:
- `200`: General error.
- `203`: Service unavailable.
- `301`: The theme file or dark theme information does not exist.

Example:

``` js 
try { 
    const res = device.getThemeSync(); 
    console.log(res. title); 
    console.log(res. author); 
} catch (e) { 
    // Do something upon an error. 
    console.log(e.code + e.message); 
}
```

### `getServiceCountryCode({success,fail,complete})`

__Obtains the current service country/region code of the device platform.__ 

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the following attribute:
  - `serviceCountryCode` (`string`). Current service country/region code of the system. It follows the [ISO 3166](https://en.wikipedia.org/wiki/ISO_3166-1) standard.
- `fail` (`function(code)`). Optional callback function for failure. This function has an argument that may have the following values:
  - `200`: General error.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.getServiceCountryCode({ 
    success: function (ret){ 
        console.log(`serviceCountryCode = ${ret.serviceCountryCode}`) 
    }, 
    fail: function(erromsg, errocode){ 
        console.log(`error message = ${erromsg}, error code = ${errocode}`) 
    }, 
    complete: function () { 
        prompt.showToast({ 
            message: "The request is completed." 
        }) 
    } 
})
```


### `toLocaleDateTimeString(timestamp)`

__Auxiliary method to convert a timestamp to a string in the same format as the system's date/time configuration__.

#### Arguments

- `timeStamp` (`number`). Mandatory argument with the timestamp to convert.

#### Return 

This method returns a `string` with the timestamp converted into the same format as the system date and time configuration. 

Example:

``` js 
console.log(device.toLocaleDateTimeString(123456))
```

### `toLocaleDateString(timestamp)`

__Auxiliary method to convert a timestamp to a string in the same format as the system's date configuration__.

#### Arguments

- `timeStamp` (`number`). Mandatory argument with the timestamp to convert.

#### Return 

This method returns a `string` with the timestamp converted into the same format as the system date and time configuration. 

Example:

``` js 
console.log(device.toLocaleDateTimeString(123456))
```

### `toLocaleTimeString(timestamp)`

__Auxiliary method to convert a timestamp to a string in the same format as the system's time configuration__.

#### Arguments

- `timeStamp` (`number`). Mandatory argument with the timestamp to convert.

#### Return 

This method returns a `string` with the timestamp converted into the same format as the system date and time configuration. 

Example:

``` js 
console.log(device.toLocaleDateTimeString(123456))
```

### `host({success,fail,complete})`

__Obtains the package name, version code, and version name of the host app__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has an `object` argument with the following attributes:
  - `package` (`string`). Package name of the host app.
  - `versionCode` (`number`). Version code of the host app.
  - `versionName` (`string`). Version name of the host app.
- `fail` (`function(code)`). Optional callback function for failure. This function has an argument that may have the following code:
  - `203`: Service unavailable.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
device.host({ 
    success: function (ret) { 
        console.log("handling success: " + JSON.stringify(ret)); 
    }, 
    fail: function (erromsg, errocode) { 
        console.log("handling success: " + errocode + ': ' + erromsg) 
    }, 
})
```

## Events

This service provides the following listener:

- [`onFontScaleChange(callback)`](#onfontscalechange-callback)

### `onFontScaleChange(callback)`

__This method is triggered after changes on the system font settings.__

#### Arguments

The listener callback function has an `object` argument that include the following attribute:
- `fontScale` (`number`). This attribute indicates the font size of the system. 

Example:

``` js 
device.onFontScaleChange({ 
    callback: function(ret) { 
        console.log("fontScale ==" + ret.fontScale); 
    } 
})
```





