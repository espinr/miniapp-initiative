# Home Screen Icon

__App shortcut installation.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.shortcut"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import shortcut from '@system.shortcut' 
```

Or

``` js
let shortcut = require("@system.shortcut")
```

## Attributes

This service has the following attribute:

- [`systemPromptEnabled`](#systempromptenabled)

### `systemPromptEnabled`

__Indicates whether to prompt the user to create a home screen icon when the user exits the app.__

- Type: `boolean`
- Value by default: `true`

Example:

``` js
// Enable the function of prompting a user to create a home screen icon. 
shortcut.systemPromptEnabled=true; 
// Disable the function of prompting a user to create a home screen icon. 
shortcut.systemPromptEnabled=false; 
// Obtain whether the function of prompting a user to create a home screen icon is enabled. 
console.log("system prompt enabled: " + shortcut.systemPromptEnabled);
```


## Methods

This service has the following methods:

- [`hasInstalled({success,fail,complete})`](#hasinstalled-success-fail-complete)
- [`install({message,success,fail,complete})`](#install-message-success-fail-complete)

### `hasInstalled({success,fail,complete})`

__Method to check if a quick app home screen icon is already in the system__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function for success. The function has a `boolean` argument indicating if the icon exists in the system (`true`) or not (`false`).
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
shortcut.hasInstalled({ 
    success: function (ret) { 
        console.log('hasInstalled success ret---' + ret); 
        if (ret) { 
            // A home screen icon has been created. 
        } else { 
            // No home screen icon has been created. 
        } 
    }.bind(this), 
    fail: function (erromsg, errocode) { 
        console.log('hasInstalled fail ret---' + erromsg); 
    }.bind(this), 
    complete: function () { 
    } 
})
```

### `install({message,success,fail,complete})`

__Method to create a home screen icon for a quick app.__.

#### Arguments

This method requires an `object` with the following attributes:
- `message` (`string`). Optional attribute with descriptive information to be displayed to the user. This is usually used for explaining the reason for this icon creation.
- `systemPromptEnabled`	boolean	Yes	Yes	Indicates whether to prompt the user to create a home screen icon when the user exits the app. The default value is true.
CAUTION
This attribute does not take when your quick app is running on Huawei Quick App Loader, but takes effect after your app is officially released.

- `success`	(`function()`). Optional callback function for success. 
- `fail` (`function(code)`). Optional callback function for failure, with the following possible code:
  - `201` when the system failed to get the permission due to the user's rejection.  
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
shortcut.install({ 
    message: 'Adds the shortcut to the desktop for convenient use next time.', 
    success: function (ret) { 
        console.log('handling createShortCut success'); 
    }, 
    fail: function (erromsg, errocode) { 
        console.log('handling createShortCut fail'); 
    }.bind(this), 
})
```
