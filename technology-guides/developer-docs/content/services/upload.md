# Upload

__File upload management.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.uploadtask"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import uploadtask from '@system.uploadtask' 
```

Or

``` js
let uploadtask = require("@system.uploadtask")
```


## Methods

This service has the following methods:

- [`uploadFile({url,files,header,method,data,timeout,success,fail,complete})`](#uploadfile-url-files-header-method-data-timeout-success-fail-complete)
- [`abort()`](#abort)

### `uploadFile({url,files,header,method,data,timeout,success,fail,complete})`

__Creates an upload request__.

Every time this method is called, a new `object` is returned.

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the resource.
- `files` (`array`). Mandatory attribute with the list of files to be uploaded, submitted as `multipart/form-data`. The array include `object`s with the following members:
  - `uri` (`string`). Mandatory attribute with the local path of the files to be uploaded. 
  - `filename` (`string`). Optional attribute with the file name in the header (when submitting `multipart`). If the file name is not specified here, the file name is obtained from the URI by default.
  - `name` (`string`). Optional attribute with the name of the form when `multipart` is submitted. The default value is `file`.
  - `type` (`string`). Optional attribute with the `Content-Type` of the files to be uploaded. If this parameter is not specified, the system will select a `Content-Type` based on the filename or URI extension. The system will show an error if `Content-Type` cannot be obtained, indicating that the parameter is invalid.
- `header` (`object`). Optional attribute with the headers of the request. User agent header is automatic specified.
- `method` (`string`). Optional attribute with the HTTP method: `POST` (default value) and `PUT`. 
- `data` (`array`). Optional attribute with additional form data in an HTTP request. The array contains objects with the following members:
  - `name` (`string`). Mandatory attribute with the name of the form field.
  - `value` (`string`). Mandatory attribute with the value of the form field.
- `timeout` (`number`). Optional attribute that specifies the connection timeout (not the read/write timeout interval) in millisecond  (by default `10000`).
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the following members:
  - `statusCode` (`number`). Server status code.
  - `data` (`string`). If the value of type in the header returned by the server is `text/*`, `application/json`, `application/javascript`, or `application/xml`, the value is textual. Otherwise, the value is the URI of a temporary file.
  - `headers` (`object`). HTTP headers in the response from the server. 
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The arguments of the callback function are:
  - `msg` (`object`) Message with information about the error (text in `msg.errMsg`).
  - `code` (`number`) Error code.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
this.mytask = uploadtask.uploadFile({ 
    url: "https://example.org/upload", 
    header: { "Accept-Encoding": "gzip, deflate", "Accept-Language": "fr-FR,en-US;q=0.8,en;q=0.6" }, 
    files: [ 
        { 
            uri: "internal://mass/videoupload.mp4", 
            name: "file1", 
            type: "video" 
        } 
    ], data: [ 
        { 
            name: "key1", 
            value: "value1" 
        } 
    ], 
    success: function (data) { 
        console.log("handling success:" + JSON.stringify(data)); 
    }, 
    fail: function (data, code) { 
        console.log("handling fail, data =" + data.statusText); 
    } 
}) 
```

### `abort()`

__Method to cancel an upload task.__

Example:

``` js 
this.mytask.abort()
```

## Events

This service provides the following listeners:

- [`onProgressUpdate(function({progress,totalBytesSent,totalBytesExpectedToSend}))`](#onprogressupdate-function-progress-totalbytessent-totalbytesexpectedtosend)
- [`offProgressUpdate()`](#offprogressupdate)
- [`onHeadersReceived(function({header}))`](#onheadersreceived-function-header)
- [`offHeadersReceived()`](#offheadersreceived)

### `onProgressUpdate(function({progress,totalBytesSent,totalBytesExpectedToSend}))`

__Method to listen to upload progress change events__.

Every time this method is called, a new `object` is returned.

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following members:
- `progress` (`number`). Upload progress, in percentage.
- `totalBytesSent` (`number`). Size of the uploaded data, in bytes.
- `totalBytesExpectedToSend` (`number`). Expected size of data to be uploaded, in bytes.

Example:

``` js 
this.mytask.onProgressUpdate(res => { 
    console.log("Upload onProgressUpdate = " + JSON.stringify(res)) 
})
```

### `offProgressUpdate()`

__Method to cancel a listener of upload progress change events__.

Example:

``` js 
this.mytask.offProgressUpdate()
```


### `onHeadersReceived(function({header}))`

__Method to listen to HTTP response header events__.

This method needs to be called before the file upload is completed.

#### Arguments

This method requires a callback `function` with an `object` as argument that has the following member:
- `header` (`object`). HTTP response headers returned by the server.

Example:

``` js 
this.mytask.onHeadersReceived(res => { 
    console.log("Upload onHeaderReceived = " + JSON.stringify(res)) 
})
```

### `offHeadersReceived()`

__Method to cancel a listener of HTTP response headers events__.

Example:

``` js 
this.mytask.offHeadersReceived()
```
