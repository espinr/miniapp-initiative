# WebView

__Use of WebView__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.webview"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import webview from '@system.webview' 
```

Or

``` js
let webview = require("@system.webview")
```


## Methods

This service has the following methods:

- [`loadUrl({url,allowthirdpartycookies,showloadingdialog})`](#loadurl-url-allowthirdpartycookies-showloadingdialog)
- [`loadCustomTabsUrl({url,options,success,fail,complete})`](#loadcustomtabsurl-url-options-success-fail-complete)
- [`preloadCustomTabsUrl({url,success,fail,complete})`](#preloadcustomtabsurl-url-success-fail-complete)
- [`getCustomTabsVersion()`](#getcustomtabsversion)
- [`system.go(path)`](#system-go-path)


### `loadUrl({url,allowthirdpartycookies,showloadingdialog})`

__This method opens a web resource__.

This service supports file download within the quick app.

By default, the app title bar will include the title of the web page loaded. 

::: tip User Agent HTTP header
The system sends a HTTP header with the following user agent:
```
hap/<platform-version-number>/<vendor-ID><platform-app-package-name>/<platform-app-version-number><app-package-name>/<app-version-number> (<source>).
```
`<source>` is the same as that the return of the [app.getInfo()](./app-context.html#getinfo) method. 
:::

::: warning
Quick apps do not differentiate HTTP and HTTPS scenarios at the underlying implementation layer, so be cautious when using HTTP links on the page.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the web page to load.
- `allowthirdpartycookies` (`boolean`). Optional flag to indicate if cookies can be delivered across domains. The default value is `false`. - `showloadingdialog` (`boolean`). Optional flag to indicate if the system displays the loading progress bar. The default value is `true`.

Example:

``` js 
webview.loadUrl({ 
    url: 'https://www.example.com', 
    allowthirdpartycookies: false 
})
```


### `loadCustomTabsUrl({url,options,success,fail,complete})`

__This method opens a web page using custom tabs__. 

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the web page to load.
- `options` (`object`). Optional parameters for customizing the appearance and style of the browser. The options may include the following attributes:
  - `toolBarColor` (`string`). Optional attribute indicating the color of the toolbar.
  - `showTitle` (`boolean`). Optional flag to indicate if the title must be displayed (`true`) or not (`false`).
  - `addDefaultShareMenuItem` (`boolean`). Optional flag to indicate if the system must add the default sharing menu (`true`) or not (`false`).
  - `navigationBarColor` (`string`). Optional attribute indicating the navigation bar color.
  - `colorScheme` (`string`). Optional attribute indicating the theme mode. The options are as follows:
    - `COLOR_SCHEME_SYSTEM`: system theme (default value)
    - `COLOR_SCHEME_LIGHT`: light theme
    - `COLOR_SCHEME_DARK`: dark theme
  - `enableUrlBarHiding` (`boolean`). Optional flag to indicate if the system must hide the address bar (`true`) or not (`false`).
  - `isOpenAdFilter` (`boolean`). Optional flag to indicate if the system must filter ads (`false` indicates that ad filtering is disabled)
  - `isOpenBlockTrackingCookies` (`boolean`). Optional flag to indicate if the system must block tracking cookies (`true` for blocking enabled and `false` for blocking disabled). The default value is `false`. 
  - `whenAboutBlankClose` (`boolean`). Optional flag to indicate if the system must close the custom tab page when the about:blank page is loaded (`true` for closing the custom tab page).
  - `userAgent` (`string`). Optional attribute indicating the type of user agent to be sent. The options are: `ANDROID_MODE`, `PC_MODE`, `IPHONE_MODE`, and `PAD_MODE`.
  - `launchFlag` (`string`). Optional attribute indicating the [mode](../guide/lifecycle.html#page-launch-mode) when using custom tabs of the browser. The only value supported is `newTask` to open custom tabs through a new task stack.
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to the failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
webview.loadCustomTabsUrl({ 
  url: 'https://www.example.org', 
  options: { 
    toolBarColor: '#FF00FF', 
    showTitle: 'false', 
    addDefaultShareMenuItem: true, 
    navigationBarColor: '#00BFFF', 
    enableUrlBarHiding: 'true', 
    isOpenAdFilter: false, 
    isOpenBlockTrackingCookies: true, 
    whenAboutBlankClose: true, 
    colorScheme: 'COLOR_SCHEME_DARK' 
  }, 
})
```

### `preloadCustomTabsUrl({url,success,fail,complete})`

__Preloads the URL of a web page.__

#### Arguments

This method requires an `object` with the following attributes:
- `url` (`string`). Mandatory attribute with the URL of the web page to load.
- `success`	(`function`). Optional callback function corresponding to the successful execution.
- `fail` (`function`). Optional callback function corresponding to the failed execution.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
webview.preloadCustomTabsUrl({ 
  url: "https://www.example.org/", 
  success: function (data) { 
    console.log(data) 
  }, 
  fail: function (data) { 
    console.error(data) 
  } 
})
```

### `getCustomTabsVersion()`

__Checks whether the custom tabs feature and other relevant features are available for use__. 

#### Return 

This method returns an `object` with the following member:
- `cctVersion` (`number`) with an integer indicating the custom tabs feature supported. The default value is `1`. 

::: tip Usage
When the value is greater than or equal to `1`, web pages can be opened using the custom tabs feature.
When the value is greater than or equal to `2`, you can use the user agent type.
:::   

Example:

``` js 
const res = webview.getCustomTabsVersion() 
console.log('CustomTabsVersion: ' + res.cctVersion)
```



### `system.go(path)`

__Redirects a user from a web page opened by the WebView to a page of the current app__. 

#### Arguments

- `path` (`string`). Mandatory argument with the path of the page to be opened. 

::: tip Usage
If the system doesn't find the page, it opens the [home page](../guide/manifest.html#entry).

Example:

``` js 
system.go("/detail?param1=value1")
```

## Example

``` html
<template> 
    <div> 
        <div> 
            <text>WebView Example</text> 
        </div> 
        <div> 
            <input type="button" onclick="loadUrl" value="Open the Quick App Initiative website." /> 
        </div> 
        <div> 
            <input type="button" onclick="loaddownLoadUrl" value="Open the web page with a download link." /> 
        </div> 
        <div> 
            <input type="button" onclick="loadFileUploadSupportUrl" value="Open the WebView to support file upload." /> 
        </div> 
        <div> 
            <input type="button" onclick="allowthirdcookie" value="Support passing cross-domain cookies." /> 
        </div> 
    </div> 
</template> 
<style> 
    @import '../Common/css/common.css'; 
    .item-container { 
        margin-bottom: 50px; 
        margin-right: 60px; 
        margin-left: 60px; 
        flex-direction: column; 
    } 
</style> 
<script> 
    import webview from '@system.webview' 
    export default { 
        loadUrl: function () { 
            webview.loadUrl({ 
                url: 'https://quick-app-initiative.ow2.io/' 
            }); 
        }, 
        loaddownLoadUrl: function () { 
            webview.loadUrl({ 
                url: 'https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/raw/master/use-cases/hello-world/src/Common/logo.png' 
            }); 
        }, 
        loadFileUploadSupportUrl() { 
            webview.loadUrl({ 
                url: "https://quick-app-initiative.ow2.io/" 
            }); 
        }, 
        allowthirdcookie: function () { 
            webview.loadUrl({ 
                url: 'https://quick-app-initiative.ow2.io/', 
                allowthirdpartycookies: true 
            }); 
        } 
    } 
</script>
```