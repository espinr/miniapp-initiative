# QR Code Scan

__QR code scan__.

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.barcode"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import barcode from '@system.barcode' 
```

Or

``` js
let barcode = require("@system.barcode")
```


## Methods

This service has the following methods:

- [`scan({success,fail,cancel,complete})`](#scan-success-fail-cancel-complete)

### `scan({success,fail,cancel,complete})`

__This method scans a QR code using the camera of the device__.

#### Arguments

This method requires an `object` with the following attributes:
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.result` is a `string` with the parsed content.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution.
  - `code` is `201` when the user rejects the request for permission to use the camera.
- `cancel` (`function()`). Optional callback function corresponding to an execution canceled by the user.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
barcode.scan({ 
  success:function(data){ 
    console.log("handling success: " + data.result); 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```


