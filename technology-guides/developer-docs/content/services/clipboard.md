# Clipboard

__Clipboard management.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.clipboard"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import clipboard from '@system.clipboard' 
```

Or

``` js
let clipboard = require("@system.clipboard")
```


## Methods

This service has the following methods:

- [`set({text,success,fail,complete})`](#set-text-success-fail-complete)
- [`get({success,fail,complete})`](#get-success-fail-complete)


### `set({text,success,fail,complete})`

__Modifies content on the clipboard__.

#### Arguments

This method requires an `object` with the following attributes:
- `text` (`string`). Mandatory attribute with the content that will be stored in the clipboard. The empty string (``) clears the clipboard value.
- `success`	(`function(res)`). Optional callback function for success.
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
clipboard.set({ 
  text: 'Text to paste!' 
})
```

### `get({success,fail,complete})`

__Retrieves the content on the clipboard__.

#### Arguments

This method requires an `object` with the following attributes:
- `text` (`string`). Mandatory attribute with the content that will be stored in the clipboard. The empty string (``) clears the clipboard value.
- `success`	(`function(res)`). Optional callback function for success with the following argument:
  - `res.text` (`string`) with the content of the clipboard.
- `fail` (`function(code)`). Optional callback function for failure. 
- `complete` (`function()`). Optional callback function for completion.


Example:

``` js 
clipboard.get({ 
  success:function(data){ 
    console.log("handling success: " + data.text); 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```