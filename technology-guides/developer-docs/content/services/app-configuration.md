# App Configuration

__Management of app configuration (locale, date, screen orientation).__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.configuration"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import configuration from '@system.configuration' 
```

Or

``` js
let configuration = require("@system.configuration")
```


## Methods

This service has the following methods:

- [`getLocale()`](#getlocale)
- [`setLocale({language,countryOrRegion})`](#setlocale-language-countryorregion)
- [`getLayoutDirection()`]()
- [`setScreenOrientation({orientation,success,fail,complete})`](#setscreenorientation-orientation-success-fail-complete)
- [`formatDateAndTime({timeStamp,flag,success,fail,complete})`](#formatdateandtime-timestamp-flag-success-fail-complete)

### `getLocale()`

Method that obtains the current locale of the app. It uses the system's locale by default. 

__Arguments__:

This method doesn't have arguments

__Return__: 

This method returns an `object` with the following members:
- `language` (`string`). The language.
- `countryOrRegion`	(`string`). Country or region.

Example:

``` js 
const locale = configuration.getLocale() 
console.log(locale.language)
```

### `setLocale({language,countryOrRegion})`

Method that sets the locale of the app.

After the execution of the method, the app changes to the new language and triggers the [`onConfigurationChanged()`](../guide/lifecycle.html#onconfigurationchanged) lifecycle method. When the system language changes or a user opens the app again, the language is reset to the system language by default. 

The request sent by the [`web element`](../components/web) and [Fetch API](./fetch) contains the `Accept-Language` HTTP header with the value of the locale.

__Arguments__:

This method requires an `object` with the following attributes:
- `language` (`string`). The language.
- `countryOrRegion`	(`string`). Country or region.

Example:

``` js 
configuration.setLocale({ 
    "language": "en", 
    "countryOrRegion": "US" 
})
```

### `getLayoutDirection()`

Method that gets the writing direction of the system.

__Arguments__:

This method doesn't have arguments.

__Return__: 

This method returns an `object` with the following members:
- `ltr` (`string`). Left-to-right.
- `rtl`	(`string`). Right-to-left.

Example:

``` js 
const dir = configuration.getLayoutDirection() 
console.log("direction="+dir)
```

### `setScreenOrientation({orientation,success,fail,complete})`

Method that sets the screen orientation.

__Arguments__:

This method requires an `object` with the following attributes:
- `orientation` (`string`). Mandatory attribute with the screen orientation. The options are `portrait`, `landscape`, and `auto`.
- `success`	(`function()`). Optional callback function for success.
- `fail` (`function()`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function on completion, regardless success or failure of the call.

Example:

``` js 
configuration.setScreenOrientation({ 
    orientation:"portrait", 
    success:function (data) { 
        console.log('result:' + data); // success 
    }, 
    fail:function () { 
    } 
})
```

::: warning
After the screen rotation, the screen width may change. As a result, the actual display is inconsistent even if you set the same size. If your app changes the screen orientation, you should use responsive layout for a better user experience.
:::

### `formatDateAndTime({timeStamp,flag,success,fail,complete})`

Method that gets a timestamp (date and time) in a string based on the current system locale and format settings.

__Arguments__:

This method requires an `object` with the following attributes:
- `timeStamp` (`number`). Mandatory attribute with the timestamp to format.
- `flag` (`number`). Mandatory attribute with the format options. You can specify multiple formats using pipe characters (`|`). For example, to display the weekday, hour, and minute, you can set `flag` to `0x00001 | 0x00002`. Common options are as follows:
  - `0x00001`: hour and minute, for example, 02:16 pm.
  - `0x00002`: weekday, for example, Friday.
  - `0x00004`: year. If this option is included, the year is always displayed.
  - `0x00010`: month and date.
  - `0x00020`: month.
- `success`	(`function(res)`). Optional callback function for success.
  - `res.dateStr` (`string`)	Timestamp in the string format.
- `fail` (`function()`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function on completion, regardless success or failure of the call.

::: tip Usage of `flag`
For more options of the `flag` parameters, check the Java [DateUtils](http://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/time/DateUtils.html) reference.
:::

Example:

``` js 
const date = Date.now() 
configuration.formatDateAndTime({ 
    timeStamp: date, 
    flag: 0x00001 | 0x00002 , 
    success: function(ret){ 
        console.log(`dateStr= ${ret.dateStr }`) 
    } 
})

```

### `simpleFormatDate({format})`

Method that gets a timestamp (date) in a string based on the current system locale and format settings.

__Arguments__:

This method requires an `object` with the following attributes:
- `timeStamp` (`number`). Mandatory attribute with the timestamp.
- `format` (`string`). Mandatory attribute with the format of the time.
- `success`	(`function(res)`). Optional callback function for success.
  - `res.dateStr` (`string`)	Timestamp in the string format.
- `fail` (`function()`). Optional callback function for failure.
- `complete` (`function()`). Optional callback function on completion, regardless success or failure of the call.

Example:

``` js 
const date = Date.now() 
configuration.simpleFormatDate({ 
    timeStamp: date, 
    format:"yyyy-mm-dd hh:mm:ss", 
    success: function(ret){ 
        console.log(`dateStr= ${ret.dateStr }`) 
    } 
})
```
