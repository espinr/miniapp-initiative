# Location

__Geolocation operations.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.geolocation"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import geolocation from '@system.geolocation' 
```

Or

``` js
let geolocation = require("@system.geolocation")
```

::: tip World Geodetic System
This service uses World Geodetic System (WGS) coordinates in the version [WGS 84](https://en.wikipedia.org/wiki/World_Geodetic_System#1984_version).
:::

## Methods

This service has the following methods:

- [`getLocation({timeout,success,fail,complete})`](#getlocation-timeout-success-fail-complete)
- [`subscribe({callback,fail})`](#subscribe-callback-fail)
- [`unsubscribe()`](#unsubscribe)
- [`getLocationType({success,fail,complete})`](#getlocationtype-success-fail-complete)

### `getLocation({timeout,success,fail,complete})`

__Method to get the current location__.

#### Arguments

This method requires an `object` with the following attributes:
- `timeout` (`long`). Optional attribute with the timeout interval, in milliseconds. The value by default is `30000`. Upon timeout, the `fail` callback is triggered.
- `success`	(`function`). Optional callback function corresponding to the successful execution. The argument of the callback function is an `object` with the members:
  - `longitude` (`float`). Longitude component of the coordinates.
  - `latitude` (`float`). Latitude component of the coordinates.
  - `accuracy` (`float`). Accuracy of the longitude and latitude.
  - `time` (`float`). Current time.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The codes could be as follows:
  - `201`: The user has rejected the request of obtaining a location.
  - `202`: Invalid parameter.
  - `203`: Service unavailable.
  - `205`: The country or region is not within the service scope.
  - `1000`: The location feature is disabled in the system settings of the device.
- `complete` (`function`). Optional callback function corresponding to the end of the execution.

Example:

``` js 
geolocation.getLocation({ 
  success:function(data){ 
    console.log("handling success: longitude=" + data.longitude + ", latitude=" + data.latitude); 
  }, 
  fail: function(data, code) { 
    console.log("handling fail, code=" + code); 
  } 
})
```

### `subscribe({callback,fail})`

__Method to listen to the current location changes__.

If you call this method more than once, only the last call will take effect.

::: warning
Do not call the [`getLocation`](#getlocation-timeout-success-fail-complete) method after you have called this method. The system will produce a timeout error.
:::

#### Arguments

This method requires an `object` with the following attributes:
- `callback` (`function`). Mandatory callback function corresponding to changes in the location. The argument of the callback function is an `object` with the members:
  - `longitude` (`float`). Longitude component of the coordinates.
  - `latitude` (`float`). Latitude component of the coordinates.
  - `accuracy` (`float`). Accuracy of the longitude and latitude.
  - `time` (`float`). Current time.
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. The codes could be as follows:
  - `201`: The user has rejected the request of obtaining a location.
  - `204`: Timeout.
  - `1000`: The location feature is disabled in the system settings of the device.
  
Example:

``` js 
geolocation.subscribe({ 
    callback:function(data) { 
        console.log("handling success: longitude=" + data.longitude + ", latitude=" + data.latitude); 
    }, 
    fail: function(data, code) { 
        console.log("handling fail, code=" + code); 
    } 
})
```

### `unsubscribe()`

__Method to cancel the listener for location changes__.

Example:

``` js 
geolocation.unsubscribe()
```

### `getLocationType({success,fail,complete})`

__Method to get the types of methods supported by the system to get the location__.

#### Arguments

This method requires an `object` with the following attributes:
- `success` (`function`). Optional callback function corresponding to the successful execution of the method. The argument of the callback function is an `object` with the member:
  - `type` (`array`) with the values of the supporting methods (e.g., `['gps', 'network']`).
- `fail` (`function(msg,code)`). Optional callback function corresponding to a failed execution. 
- `complete` (`function`). Optional callback function corresponding to the end of the execution.
  
Example:

``` js 
geolocation.getLocationType({ 
    success: function (data) { 
        console.log("data - "+data.types) 
    }, 
    fail: function (erromsg, errocode) { 
        console.log('getLocationType: ' + errocode + ': ' + erromsg) 
    } 
})
```
