# SMS Messages

__SMS (texts) distribution.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.sms"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import sms from '@system.sms' 
```

Or

``` js
let sms = require("@system.sms")
```


## Methods

This service has the following method:

- [`send({address,content,success,fail,complete})`](#send-address-content-success-fail-complete)

### `send({address,content,success,fail,complete})`

__Sends SMS messages__.

Every time this method is called, the system will show a confirmation prompt. SMS messages contains up to 70 characters.

#### Arguments

This method requires an `object` with the following attributes:
- `address` (`string`). Mandatory attribute with the destination phone number to which the SMS message will be sent. (The value may start with a plus character (`+`) and may contain numeric characters and spaces.)
- `success`	(`function()`). Optional callback function for success. 
- `fail` (`function(data, code)`). Optional callback function for failure. Some of the codes that can be produced:
  -  `200`: Sending failed.
  -  `201`: The user has rejected the request of sending an SMS message.
  -  `202`: The parameter is empty or invalid.
- `complete` (`function()`). Optional callback function for completion.

Example:

``` js 
message.send({ 
    address:'198900086', 
    content:'This is the SMS message content.', 
    success: function () { 
        console.log('handling success') 
     }, 
    fail: function (data, code) { 
        console.log(`handling fail, code = ${code}`) 
    } 
})
```