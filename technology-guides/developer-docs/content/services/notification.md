# Notifications

__Display of notifications to the user.__

## Manifest Declaration

You need to declare the use of this API in the [manifest's `features`](../guide/manifest.html#features) member:

``` json
{"name": "system.notification"}
```

## Module Import

Before using this service in a component, you need to [import the module](../guide/scripting.html#import-and-export-modules) in the script section of the [UX document](../guide/ux-documents).

``` js
import notification from '@system.notification' 
```

Or

``` js
let notification = require("@system.notification")
```

## Methods

This service has the following methods:

- [`show({contentTitle,contentText,clickAction})`](#show-contenttitle-contenttext-clickaction)
- [`postDelayedShow({package,success,fail,complete})`](#postdelayedshow-package-success-fail-complete)
- [`cancelDelayedShow({jobId,success,fail,complete})`](#canceldelayedshow-jobid-success-fail-complete)


### `show({contentTitle,contentText,clickAction})`

__This method displays a notification.__

#### Arguments

This method requires an `object` with the following attributes:
- `contentTitle` (`string`). Mandatory text with the title of the notification.  
- `contentText`	(`string`). Mandatory text with the content of the notification.
- `clickAction`	(`object`).	Optional `object` with the action triggered when the user clicks on a notification. The object may contain the following attribute:
  - `uri` (`string`). Mandatory parameter with the URI of the page that will be shown after a notification is tapped. The following formats are supported:
    - Absolute in-app page path (e.g., `/about`);
    - Relative in-app page path (e.g., `About`).
    - `/`: If there is a page with `/` as a path, it is displayed; otherwise, the system will use the home page by default.

::: tip
You can use query string parameters (e.g., `?param1=value1`). Once in the new page, you can access the value of the parameter directly on its instance (e.g., `this.param1`).
:::

::: warning
If the user doesn't grant the permission for notifications, this method doesn't take effect.
:::

Example:

``` js 
notification.show({ 
  contentTitle: 'Congrats!!',
  contentText: 'You can now redeem the discount code' 
  clickAction: { 
    uri:'/index.html?index=1' 
  } 
})
```

### `postDelayedShow({message,buildInfo,success,fail,complete})`

__This method displays a notification with a delay.__

#### Arguments

This method requires an `object` with the following attributes:
- `message` (`object`). Mandatory attribute with the details of a notification, including the following members:
  - `contentTitle` (`string`). Mandatory text with the title of the notification.  
  - `contentText`	(`string`). Mandatory text with the content of the notification.
  - `clickAction`	(`object`).	Optional `object` with the action triggered when the user clicks on a notification. The object may contain the following attribute:
    - `uri` (`string`). Mandatory parameter with the URI of the page that will be shown after a notification is tapped. The following formats are supported:
      - Absolute in-app page path (e.g., `/about`);
      - Relative in-app page path (e.g., `About`).
      - `/`: If there is a page with `/` as a path, it is displayed; otherwise, the system will use the home page by default.
- `buildInfo` (`object`). Mandatory attribute with the configuration for the delay, with the following members:
  - `latencyTime` (`long`). Mandatory integer with the delay time, in seconds.
  - `isPersisted` (`boolean`). Mandatory `boolean` that indicates whether notifications are persisted even when a device is restarted. 
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
  - `res.jobId` is a `number` that indicates the identifier of a delayed message. This identifier can be used to cancel a notification.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.


Example:

``` js
notification.postDelayedShow({ 
    message:{ 
           contentTitle: 'title', 
           clickAction: { 
            uri: '/index.html?index=1' 
           } 
    }, 
    buildInfo:{ 
           latencyTime: 1*60,  // 1 minute 
           isPersisted:false 
    }, 
    success:function(params){ 
          console.info("success, id is "+ params.jobId) 
    } 
}) 
```

### `cancelDelayedShow({jobId,success,fail,complete})`

__This method cancels displaying notifications with a delay.__

#### Arguments

This method requires an `object` with the following attributes:
- `jobId` (`integer`). Mandatory attribute that indicates the identifier of a delayed message.
- `success`	(`function(res)`). Optional callback function corresponding to the successful execution.
- `fail` (`function(res, code)`). Optional callback function corresponding to the failed execution.
- `complete` (`function()`). Optional callback function corresponding to the end of the execution.

Example:

``` js
notification.cancelDelayedShow({ 
    jobId:12, 
    success:function(params){ 
          console.info("success") 
    } 
})
```
