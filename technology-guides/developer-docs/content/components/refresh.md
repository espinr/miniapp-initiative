# `refresh`

[Pull-to-refresh](https://en.wikipedia.org/wiki/Pull-to-refresh) mechanism.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/refresh01.gif" alt="Refresh component on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element supports any [element](./) as children nodes.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.


- [`offset`](#offset)
- [`refreshing`](#refreshing)
- [`type`](#type)
- [`indicator`](#indicator)


### `offset`

Distance of the refresh element to the top of the viewport where the element is not moved.

- Type: `string` (length) 
- Default value: `132px` 
- Mandatory: no 

### `refreshing`

Flag that indicates if an element is being refreshed.

- Type: `boolean`
- Default value: `false` 
- Mandatory: no 

### `type`

Type of the behavior of the system refresh action.

- Type: `string` (`material` | `overscroll`)
- Default value: `material` 
- Mandatory: no 

The value `material` indicates that the page stops moving when a user continues pulling after the boundary is reached. On the other hand, `overscroll` enables the page to move a little beyond that limit, showing a bouncing effect.

### `indicator`

Flag that indicates whether an item is an indicator.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`background-color`](#background-color)
- [`progress-color`](#progress-color)

### `background-color` 

Background color.

- Type: `<color>`  
- Default value: `white` 
- Mandatory: no 


### `progress-color` 

Loading animation foreground color. 

- Type: `<color>`  
- Default value: `black` 
- Mandatory: no 

## Events

In addition to the [common events](./events), this element supports the following events:

- [`refresh({refreshing})`](#refresh-refreshing)

### `refresh({refreshing})` 

Event triggered with a user pulls down a refresh element. 

__Additional parameters__:
- `object` with the following attributes:
  - `{ refreshing: number }`


## Methods

This element does not have additional methods.


## Example

``` html
<template>
      <div class="container mt-item">
      <refresh class="refresh" refreshing="{{isRefreshing}}" onrefresh="refresh">
        <div class="item-container tip-container row-center column-center">
          <text class="color-secondary">Pull down to refresh</text>
        </div>
        <div class="mlr-container row-center mt-btn">
          <input type="button" value="Stop Refreshing" class="btn-blue" onclick="stopRefresh"/>
        </div>
      </refresh>
    </div>
</template>

<style lang="sass">
    .tip-container{
      height: 400px;
      background-color: transparent;
    }
    .refresh{
      flex-direction: column;
    }
</style>

<script>
  module.exports = {
    public: {
        isRefreshing:false, 
    },
    stopRefresh(){
      this.isRefreshing = false;
    },
    refresh(e){
      this.isRefreshing = e.refreshing;
      setTimeout(() => {
        this.isRefreshing = false;
      },3000);
    }
  }
</script>
```
