# `swiper`

Slideshow or carousel container.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/swiper01.gif" alt="Swiper component on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element supports [any element](./) as children nodes, except [`list` elements](./list).

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`index`](#index) 
- [`autoplay`](#autoplay) 
- [`interval`](#interval) 
- [`indicator`](#indicator) 
- [`loop`](#loop) 
- [`duration`](#duration) 
- [`vertical`](#vertical) 
- [`previousmargin`](#previousmargin) 
- [`nextmargin`](#nextmargin) 



### `index`

Index of the currently displayed item in the list of children elements.

- Type: `number` 
- Default value: `0` 
- Mandatory: no 

### `autoplay`

Flag that indicates if the component displays all the elements automatically without user interaction.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 
 
### `interval`

Interval (in milliseconds) for displaying the elements during the `autoplay` animation.

- Type: `number` 
- Default value: `3000` 
- Mandatory: no 

### `indicator`

Flag that indicates whether to enable an indicator with different options to display. This is used to inform the user the position of the current displayed element in the list of options. 

- Type: `boolean` 
- Default value: `true` 
- Mandatory: no 

### `loop`

Flag that indicates if the slideshow is in an infinite loop.

- Type: `boolean` 
- Default value: `true` 
- Mandatory: no 


### `duration`

Length of a complete animation cycle.

- Type: `number` 
- Default value: - 
- Mandatory: no 

 
### `vertical`

Flag to indicate the direction of the swipe component (horizontal by default).

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 
 
### `previousmargin`

Visual margin to show a small part of the previous item in the slideshow cycle. 

- Type: `string` (length in `px` and percentage `%`)
- Default value: `0px` 
- Mandatory: no 

### `nextmargin`

Visual margin to show a small part of the next item in the slideshow cycle. 

- Type: `string` (length in `px` and percentage `%`)
- Default value: `0px` 
- Mandatory: no 

::: tip
The sum of [`previousmargin`](#previousmargin) and [`nextmargin`](#nextmargin) cannot exceed the half of the total component size. Otherwise, the system will truncate the exceeding part.
::: 

## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`indicator-color`](#indicator-color) 
- [`indicator-selected-color`](#indicator-selected-color) 
- [`indicator-size`](#indicator-size) 
- [`indicator-top|left|right|bottom`](#indicator-top-left-right-bottom)

### `indicator-color` 

Color of the indicator (marker with the visual index of the elements in the slideshow). 

- Type: `<color>`  
- Default value: `rgba(0,0,0,0.50)` 
- Mandatory: no 


### `indicator-selected-color` 

Color of the indicator currently selected (marker with the visual index of the elements in the slideshow). 

- Type: `<color>`   
- Default value: `rgba(51,180,255,1.0)` 
- Mandatory: no 

### `indicator-size` 

Diameter of an indicator. 

- Type: `<length>`  
- Default value: `20px` 
- Mandatory: no 

### `indicator-top|left|right|bottom` 

Position of the indicator relative to the swiper component. 

- Type: `<length-percentage>`  
- Default value: - 
- Mandatory: no 


## Events

In addition to the [common events](./events), this element supports the following event:

- [`change`](#change)


### `change` 

This event is triggered when the element displayed in the swiper changes.

__Additional parameters__: 
- `{ index: number }`

## Methods

This element has the following method:

- [`swipeTo({index})`](#swipeto-index)

### `swipeTo({index})`

Select the element specified by the index on the slideshow.

__Parameters__:
- `object` with the following attributes:
  - `index: number` (mandatory). Index with the position to display

Example:

``` js
this.$element('swiper').swipeTo({ index:2 })	
```

## Example

``` html
<template>
  <div class="container">
    <div class="mt-item mlr-container bro-s">
      <swiper style="height:282px;" interval="{{intervalTime}}" duration="{{animationTime}}" loop="{{isLoop}}" autoplay="{{isAutoplay}}" indicator="{{showIndicator}}" class="swiper">
        <div class="swiper-item bg-green"></div>
        <div class="swiper-item bg-gray"></div>
        <div class="swiper-item bg-blue"></div>
      </swiper>
    </div>
    <div class="info-container" style="margin-top: 24px;">
      <div class="info-item border-bottom-1">
        <text class="title">indicator</text>
        <div class="result">
          <switch checked="{{showIndicator}}" onchange="showIndicatorChange"></switch>
        </div>
      </div>
      <div class="info-item border-bottom-1">
        <text class="title">auto play</text>
        <div class="result">
          <switch checked="{{isAutoplay}}" onchange="isAutoplayChange"></switch>
        </div>
      </div>
      <div class="info-item">
        <text class="title">cycle through</text>
        <div class="result">
          <switch checked="{{isLoop}}" onchange="isLoopChange"></switch>
        </div>
      </div>
    </div>
    <div class="mt-item item-container">
      <div class="info-item-container2">
        <div class="top">
          <text class="name color-primary">autoplay interval (ms)</text>
          <text class="value color-secondary">{{intervalTime}}</text>
        </div>
        <div class="bottom row-center">
          <slider class="slider" min="0" max="3000" step="1" value="{{intervalTime}}" onchange="intervalTimeChange"></slider>
        </div>
      </div>
      <div class="info-item-container2">
        <div class="top">
          <text class="name color-primary">animation duration (ms)</text>
          <text class="value color-secondary">{{animationTime}}</text>
        </div>
        <div class="bottom row-center">
          <slider class="slider" min="0" max="3000" step="1" value="{{animationTime}}" onchange="animationTimeChange"></slider>
        </div>
      </div>
    </div>
  </div>
</template>

<style lang="sass">
  .info-item-container2 {
    flex-direction: column;
    .top {
      height: 100px;
      .name {
        width: 100%;
      }
      .value {
        flex-basis: 200px;
        text-align: right;
      }
    }
    .bottom {
      height: 80px;
      flex-direction: column;
    }
  }
</style>

<script>
  module.exports = {
    public: {
      showIndicator: true,
      isAutoplay:false,
      isLoop:true,
      animationTime:300,
      intervalTime:1000,
    },
    showIndicatorChange({ checked }) {
      this.showIndicator = checked;
    },
    isAutoplayChange({checked}){
      this.isAutoplay = checked;
    },
    isLoopChange({checked}){
      this.isLoop = checked;
    },
    animationTimeChange({progress}){
      this.animationTime = progress;
    },
    intervalTimeChange({progress}){
      this.intervalTime = progress;
    }
  }
</script>
```