# `richtext`

Content coming soon...

Visit our [repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) and [raise an issue](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) if you want to contribute. Thanks you for your collaboration

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/richtext01.jpg" alt="Richtext on a Quick App" /> 

([Example code](#example))


## Example

``` html
<template>
      <div class="container">
        <div class="item-container mt-item">
          <richtext type="html">{{htmlText}}</richtext>
        </div>
    </div>
</template>

<script>
  module.exports = {
    public: {
        htmlText:'<p class="item-title">h1</p><h1>Quick App</h1><p class="item-title">h2</p><h2>Quick App</h2><p class="item-title">h3</p><h3>Quick App</h3><p class="item-title">h4</p><h4>Quick App</h4><p class="item-title">h5</p><h5>Quick App</h5><p class="item-title">h6</p><h6>Quick App</h6>' 
    },
  }
</script>
```