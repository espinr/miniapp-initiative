# `popup`

Displays a tooltip text on an element.

The content in the pop-up element is a `text` specified as a child node. 

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/popup01.gif" alt="Pop up component on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element only supports [`text` elements](./text) elements as children nodes.

## Attributes


In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`target`](#target)
- [`placement`](#placement)

### `target`

This attribute identifies the target element (its `id`) where the pop-up will be shown.

- Type: `string` 
- Default value: - 
- Mandatory: yes 

### `placement`

This attribute defines the position of the pop-up.

- Type: `string` (`left` | `right` | `top` | `bottom` | `topLeft` | `topRight` | `bottomLeft` | `bottomRight`)
- Default value: `bottom` 
- Mandatory: no 


## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling property:

- [`mask-color`](#mask-color)

### `mask-color` 

Color of the overlay mask.  

- Type: `<color>`  
- Default value: `#000000` 
- Mandatory: no 

::: tip
If the value is not specified, the mask will be completely transparent.
If only the color is specified, the opacity is set to `30%` by default.
:::

## Events

In addition to the [common events](./events) (with the exception of [`appear`](events.html#appear) and [`disappear`](events.html#disappear), this element supports the following event:

- [`visibilitychange`](#visibilitychange)

### `visibilitychange` 

This event is triggered when a pop-up becomes visible or hidden.

__Additional parameters__: 
- `{ visible: boolean }`


## Methods

This element doesn't support additional methods.

## Example

``` html
<template>
  <div class="container column-center row-center">
    <div class="mlr-container">
      <input type="button" value="pop up:top-pop" class="btn-blue"  id="pop-top"/>
    </div>
    <div class="mlr-container mt-btn">
      <input type="button" value="pop up:bottom-pop" class="btn-blue"  id="pop-bottom"/>
    </div>
    <popup target="pop-top" placement="top" style="mask-color: #333333;">
      <text>top</text>
    </popup>
    <popup target="pop-bottom" style="mask-color: #333333;">
      <text>bottom</text>
    </popup>
  </div>
</template>

<script>
  module.exports = {
  }
</script>
```