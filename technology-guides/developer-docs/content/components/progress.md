# `progress`

Progress indicator.

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/progress01.gif" alt="Progress statuses on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

This element doesn't support children elements.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`percent`](#percent)
- [`type`](#type)

### `percent`

This attribute indicates the value of the progress as a percentage. 

- Type: `number` 
- Default value: `0` 
- Mandatory: no 

::: tip
This attribute is invalid when [`type`](#type) is `circular`. 
Decimal values will be rounded off to integers.
:::

### `type`

Type of the progress bar. 

- Type: `string` (`horizontal` | `circular`)
- Default value: `horizontal` 
- Mandatory: no 

::: warning
The value of this attribute cannot be modified dynamically.
:::

## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`color`](#color)
- [`stroke-width`](#stroke-width)
- [`layer-color`](#layer-color)

This element supports the [`:active` pseudo-class](../guide/styling.html#css-selectors).

### `color` 

Color of the progress bar.

- Type: `<color>`  
- Default value: `#33b4ff` 
- Mandatory: no 

### `stroke-width` 

Width of a horizontal progress bar.

- Type: `<length>`  
- Default value: `32px` 
- Mandatory: no 

### `layer-color` 

Background color of a progress bar.

- Type: `<color>`  
- Default value: `#f0f0f0` 
- Mandatory: no 

::: tip
-	For a `horizontal` progress bar, the background color by default is `#f0f0f0`.
-	For a `circular` progress bar, the `width` and `height` are `32px` by default. If the `width` and `height` are different, the smaller value will be applied to both properties.
- To set the width of a progress bar, you should place the `progress` element into a [`div`](./div) element and set the `div`'s width. 
:::


## Events

This element support all the [common events](./events), with the exception of [`swipe`](events.html#swipe).

## Methods

This element does not have additional methods.

## Example

``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">type="horizontal"</text>
    </div>
    <div class="item-container">
      <div class="progress-box row-center">
        <progress percent="40" class="horizontal-progress"></progress>
      </div>
    </div>
    <div class="case-title mt-item">
        <text class="title">type="circular"</text>
      </div>
    <div class="item-container">
      <div class="progress-box row-center column-center">
        <progress type="circular" class="circular-progress"></progress>
      </div>
    </div>
  </div>
</template>

<style lang="sass">
  .progress-box {
    width: 100%;
    height: 200px;
    flex-direction: column;
  }
  .horizontal-progress {
    stroke-width: 10px;
  }
  .circular-progress {
    width: 70px;
    height: 70px;
  }
</style>
```