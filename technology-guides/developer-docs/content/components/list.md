# `list`

List items container. 

<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/list01.gif" alt="List on a Quick App" /> 

([Example code](#example))

[[toc]]

## Children Elements

Only the [`list-item`](./list-item) elements are supported as children nodes.

## Attributes

In addition to the [common attributes](./attributes), this element may contain the following attributes.

- [`scrollpage`](#scrollpage)

### `scrollpage`

Page scroll activation/deactivation.

- Type: `boolean` 
- Default value: `false` 
- Mandatory: no 

Activate/deactivate the scroll on the rest of the page elements, including those out of the list. Enabling this attribute will compromise the list rendering performance. 

::: warning
The `scrollpage` attribute is not supported if the list is horizontal.
:::

## CSS Properties

In addition to the [common styles](./styles), this element supports the following styling properties:

- [`flex-direction`](#flex-direction)
- [`columns`](#columns)
- [`layout-type`](#layout-type)


### `flex-direction` 

Sets the direction of the main flex container and specifies how the `list-item` children are placed in the `list`. 

- Type: `string` (`column`, `row`, `row-reverse`, `column-reverse`) 
- Default value: `column` 
- Mandatory: no 


### `columns`

Number of displayed columns. 

- Type: `number` 
- Default value: `1` 
- Mandatory: no 

### `layout-type` 

Type of the layout of the list.

- Type: `string` (`grid`, `stagger`) 
- Default value: `grid` 
- Mandatory: no 

The layout style could be `grid`, in case of a grid style, and `stagger` for a waterfall style (where the height of each `list-item` can be customized).

## Events

In addition to the [common events](./events), this element supports the following events:

- [`scroll`](#scroll)
- [`scrollbottom`](#scrollbottom)
- [`scrolltop`](#scrolltop) 
- [`scrollend`](#scrollend) 
- [`scrolltouchup`](#scrolltouchup)

### `scroll` 

Indicates if the list is scrolling. 

__Additional parameters__: 
- `{ scrollX: integer, scrollY: integer, scrollState: integer }`

The state of the scroll (`scrollState`) could be: 
- `0` when the list is not scrolling, 
- `1` when the list is scrolling, and 
- `2` when the list is scrolling and the user already is interacting with the list.

### `scrollbottom` 

A list is scrolled to the bottom.

__Additional parameters__: no

### `scrolltop` 

A list is scrolled to the top.

__Additional parameters__: no

### `scrollend` 

A list stops scrolling.

__Additional parameters__: no

### `scrolltouchup` 

A list is scrolling inertially after the user stops the interaction with the list.

__Additional parameters__: no

## Methods

This element has the following methods:

- [`scrollTo({index, smooth, behavior})`](#scrollto-index-smooth-behavior)
- [`scrollBy({dx, dy, smooth} | {left, top, behavior})`](#scrollby-dx-dy-smooth-left-top-behavior)

### `scrollTo({index, smooth, behavior})`

Scrolls a list to the position of a specified `list-item`.

__Parameters__:
- `object` with the following attributes:
  - `index: integer` (mandatory). The index of the target `list-item` to scroll to.
  - `smooth: boolean`. To enable smooth scrolling (`false` by default).
  - `behavior: string`. Equivalent to the `smooth` parameter. The value can be: `smooth`, `instant`, or `auto` (`auto` by default, that behaves as `instant`). 

### `scrollBy({dx, dy, smooth} | {left, top, behavior})` 

Scrolls a list using an offset.

__Parameters__:
- Either an `object` with the following attributes:
  - `dx: integer`. Horizontal scrolling offset in `px` (`0` by default).
  - `dy: integer`. Vertical scrolling offset in `px`.
  - `smooth: boolean`. Scroll a list smoothly or not (`true` by default).
- Or an `object` with the following attributes:
  - `left: integer` . Horizontal scrolling offset in `px` (`0` by default). If the value is positive, the list scrolls to the left. If the value is negative, the list scrolls to the right. This parameter does not take effect when [`flex-direction`](#flex-direction) is set to `column` or `column-reverse`.
  - `top: integer` Vertical scrolling offset in `px` (`0` by default). If the value is positive, the list scrolls upwards. If the value is negative, the list scrolls downwards. This parameter does not take effect when [`flex-direction`](#flex-direction) is set to `row` or `row-reverse`.
  - `behavior: string` | Indicates whether to enable smooth scrolling. Value can be `smooth`, `instant`, or `auto` (by default which behaves like `instant`).

## Example
``` html
<template>
  <div class="container">
    <div class="case-title mt-item">
      <text class="title">grid layout</text>
    </div>
    <div class="item-container">
      <list class="list grid" onscrollbottom="gridScrollBottom" id="list">
        <list-item class="list-item bg-blue" type="grid" for="(index,gridItem) in list1">
          <text class="color-white">{{index}}</text>
        </list-item>
        <list-item class="list-item" type="grid">
          <progress class="progress" type="circular"></progress>
        </list-item>
      </list>
    </div>
    <div class="case-title mt-item">
      <text class="title">waterfall layout</text>
    </div>
    <div class="item-container">
      <list class="list stagger" onscrollbottom="staggerScrollBottom" id="listStagger">
        <list-item class="list-item bg-blue" style="height:{{gridItem.height}}px;" type="stagger" for="(index,gridItem) in list2">
          <text class="color-white">{{index}}</text>
        </list-item>
        <list-item class="list-item" type="stagger">
          <progress class="progress" type="circular"></progress>
        </list-item>
      </list>
    </div>
    <div class="row-center mt-btn">
      <text class="btn-transparent" onclick="scrollTop">Back to the top</text>
    </div>
    <div class="row-center mt-btn">
      <text class="btn-transparent" onclick="scrollDown">200px smooth down</text>
    </div>
  </div>
</template>

<style lang="sass">
  @import '../../../Common/css/common.scss';
  @import '../../../Common/css/button.scss';
  .list {
    height: 300px;
    background-color: #f1f1f1;
    columns: 4;
    .list-item {
      margin-right: 10px;
      margin-bottom: 10px;
      justify-content: center;
    }
    .progress {
      width: 50px;
      height: 50px;
    }
  }
  .grid {
    .list-item {
      height: 100px;
    }
  }
  .stagger {
    layout-type: stagger;
  }
</style>

<script>
  const listTmp = [
    { height: 80 },
    { height: 170 },
    { height: 100 },
    { height: 70 },
    { height: 250 },
    { height: 150 },
    { height: 120 },
    { height: 70 },
    { height: 150 },
    { height: 80 }
  ];
  const listTmp2 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  module.exports = {
    public: {
      list1: [...listTmp2, ...listTmp2],
      list2: [...listTmp, ...listTmp]
    },
    onInit: function () {
      this.$page.setTitleBar({ text: 'list' });
    },
    gridScrollBottom() {
      setTimeout(() => {
        this.list1 = [...this.list1, ...listTmp2];
      }, 1000);
    },
    staggerScrollBottom() {
      setTimeout(() => {
        this.list2 = [...this.list2, ...listTmp];
      }, 1000);
    },
    scrollDown() {
      this.$element('list').scrollBy({ dy: 200 });
      this.$element('listStagger').scrollBy({ dy: 200 });
    },
    scrollTop() {
      this.$element('list').scrollTo({ index: 0 });
      this.$element('listStagger').scrollTo({ index: 0 });
    }
  }
</script>
```

::: tip
Sometimes, the `scrollto` method may fail to scroll a list to the specified location. The possible cause is that the element was not rendered yet. You can guarantee that performance, calling the method after 50 milliseconds.
:::
