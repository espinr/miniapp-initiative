# `transform` property

Content coming soon...

Visit our [repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) and [raise an issue](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) if you want to contribute. Thanks you for your collaboration


<img style="height: 500px; border-radius: 6px; border: 1px #333333 solid" src="./images/transform01.gif" alt="Transform options on a Quick App" /> 

([Example code](#example))


## Example

``` html
<template>
  <div class="container">
    <div class="item-container">
      <div class="transform-container row-center column-center">
        <div class="transform-item" style="transform: rotate({{rotateNum}}deg) rotateX({{rotateXNum}}deg) rotateY({{rotateYNum}}deg) translateX({{translateXNum}}px) translateY({{translateYNum}}px) scale({{scaleNum}}) scaleX({{scaleXNum}}) scaleY({{scaleYNum}});"></div>
      </div>
    </div>
    <list class="mlr-container btn-list">
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="rotate">rotate</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="rotateX">rotateX</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="rotateY">rotateY</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="translate">translate</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="translateX">translateX</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="translateY">translateY</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="scale">scale</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="scaleX">scaleX</text>
      </list-item>
      <list-item type="btn" class="btn-item">
        <text class="btn-transparent" onclick="scaleY">scaleY</text>
      </list-item>
    </list>
  </div>
</template>

<style lang="sass">
  .transform-container {
    height: 400px;
  }
  .transform-item {
    width: 100px;
    height: 100px;
    background-color: #00bfff;
    border: 2px solid #000000;
  }
  .btn-list {
    columns: 3;
    .btn-item {
      height: 140px;
      align-items: center;
      justify-content: center;
    }
  }
</style>

<script>
  module.exports = {
    public: {
      rotateNum: 0,
      rotateXNum: 0,
      rotateYNum: 0,
      translateXNum:0,
      translateYNum:0,
      scaleXNum:1,
      scaleYNum:1,
      scaleNum:1,
    },
    rotate() {
      this.rotateNum = (this.rotateNum + 20) % 360;
    },
    rotateX() {
      this.rotateXNum = (this.rotateXNum + 20) % 360;
    },
    rotateY() {
      this.rotateYNum = (this.rotateYNum + 20) % 360;
    },
    translateX(){
      this.translateXNum = this.translateXNum + 10;
    },
    translateY(){
      this.translateYNum = this.translateYNum + 10;
    },
    translate(){
      this.translateYNum = this.translateYNum - 10;
      this.translateXNum = this.translateXNum - 10;
    },
    scale(){
      this.scaleXNum = this.scaleXNum - 0.1;
      this.scaleYNum = this.scaleYNum - 0.1;
    },
    scaleX(){
      this.scaleXNum = this.scaleXNum + 0.1;
    },
    scaleY(){
      this.scaleYNum = this.scaleYNum + 0.1;
    }
  }
</script>
```