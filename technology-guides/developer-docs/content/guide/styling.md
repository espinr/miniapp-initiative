# App Styling

Stylesheets and inline styles determine the appearance of the components and their elements and the rules on how the quick app engine renders the app pages. Styling of quick apps is based on a [CSS](https://www.w3.org/TR/CSS/) profile and supports preprocessing with [LESS](https://lesscss.org/) and [Saas](https://sass-lang.com/).   

## CSS Flexible Box

The layout is based on the [CSS Flexible Box](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout) model, to optimize the user interface design, and the distribution of items in one dimension.

The quick app engine adapts the size of the elements of the pages depending on the capabilities of the device and based on the [reference viewport width](./manifest#designwidth) specified in the manifest. Thus, all the styles that affect dimensions of elements (e.g., `width` and `font-size`) are initially defined based on that reference width (by default, `750 px`) and scaled to the device viewport dimensions. 

::: tip EXAMPLE
Considering `designwidth: 750` (default value) as reference value and a 1500 px-wide display, an element with `width: 100 px` will be automatically resized to `width: 200 px` (i.e., the viewport has double size of the reference width).
:::

## Define Styles

The UX documents that defines components may include a dedicated `<style>` section where you can define the locally-scoped CSS rules to be applied on the component.

For instance:

``` html {7,17}
<template>
  <div class="container">
    <text class="title">Hi Quick Apps!!</text>
  </div>
</template>

<style>
  .container {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .title {
    font-size: 100px;
    color: #191970;
  }
</style>

<script>
  module.exports = {
    data: {
      componentData: {},
    }
  }
</script>
```

### Apply Styles to Components

All the built-in [essential components](../components) support the `style` and `class` attributes, like in HTML elements to define their style.

For instance:

``` html
<!-- Inline style --> 
<div style="color:red; margin: 10px;" /> 

<!-- Class declaration --> 
<div class="normal append" />
```

### Import Stylesheets

You also may use external CSS documents that can be imported by the component that will use them. You might also import stylesheets in the `app.ux` document in case you need to apply stylesheets globally.  

You can use two different import methods:

Import an external file, replacing the styles defined in `<style>`. For instance:

``` html
<style src="./style.css"></style> 
```

::: warning
Only one `<style>` element is allowed in a `.ux` document.
:::

Merge the content of the external stylesheet with the styles declared in the `<style>` element. For instance:

``` html
<style> 
  @import "../Common/base.css";
  .title {
    font-size: 100px;
  }
</style>
```

## CSS Selectors

Quick app supports a subset of the standard CSS selectors (the first parts of CSS rules), as summarized in the following table. 

| Selector | Example | Description |
| --------------- | ------- | ----------- |
| Type selector | `div` | Selector that targets the element type (e.g., `<div>...`). |
| Class selector | `.header { }` | Selector that targets the value of a `class` attribute (e.g., `<element class="header">...`). |
| ID selector 	| `#name { }` | Selector that targets the value of an `id` attribute (e.g., `<element id="name">...`). |
| Descendant combinator | `.header video { }` | Combination of two selectors. Elements matched by the second selector are selected if they have an ancestor (parent, parent's parent, parent's parent's parent,...) element matching the first selector. (e.g., `<div class="header"><div><video>...`). |
| Child combinator | `.header > .title { }` | Combination of two selectors that matches only those elements matched by the second selector that are the direct children of elements matched by the first. (e.g., `<div class="header"><div class="title">...`). |
| Selector lists | `div, input { }` | Combination of rules for the list of selectors separated by a comma (`,`) between them. |

Examples of selectors:

``` html
<style> 
    // Direct selectors 
    text { } 
    .class-abc { } 
    #idAbc { } 

    // Descendant combinator 
    .doc-page #testTag div text { } 
    .doc-page #test-class .class1 { } 
    .doc-page #testId #testIdId1 { }

    // Child combinator 
    .doc-page #testTag .flex-column > text { }

    // Lists of selectors. 
    .font-text, .font-comma { } 
</style>
```


### Pseudo-classes and Pseudo-elements

Quick app support selectors for pseudo-classes, which style certain states of elements (e.g., the `:focus` pseudo-class selects an element when it receives the focus).

For instance, the following CSS rules apply on a button. The selector `.btn:disabled` applies only to the button when the attribute `disabled` is `true`:

``` html
<template> 
  <input type="button" class="btn" disabled="{{btndisabled}}">Click</input> 
</template> 
<style> 
  .btn { 
    width: 360px; 
    height: 180px; 
    background-color: red; 
  } 
  .btn:disabled { 
    background-color: green; 
  } 
</style>
```

Quick app stylesheets support the following pseudo-classes:

| Pseudo-Class | Supporting Element | Conditions for Matching |
| ------------ | ------------------ | ----------------------- |
| `:active` | [All](../components/) | The user touches or select an element. The condition changes when the pointer leaves the element. |  
| `:disabled` | [All](../components/) | The value of the attribute `disabled` is `true`. |
| `:focus` | [input](../components/input), [textarea](../components/textarea) |  The element obtains the focus. |
| `:checked` | [input(type="checkbox")](../components/input#checkbox), [switch](../components/switch) | The value of the attribute `checked` is `true`. |

### Selector Priority

Styling in quick apps follows the standard [CSS Cascade and inheritance](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance) rules. 

For instance, the following selectors are declared in by priority (lower to higher):

1. `#page {}`
2. `#page .class-div {}`
3. `#page .class-div text {}`
4. `#page #body .container div text {}`

If two declarations have the same level of priority, the latter overrides the former by the cascade principle.

For instance, the following example declares three combination of selectors. 

``` html
<template> 
  <div class="{{titleClass}}"> 
    <text class="{{textClass}}">Description</text> 
  </div> 
</template> 
<style> 
  .class1 .class2 { 
    color: #ff0000;  /* Red */ 
  } 
  .class1 .class2 { 
    color: #0000ff;  /* Blue */
  } 
  .class2 .class1 { 
    color: #00ff00;  /* Green */
  } 
</style> 
<script> 
    export default { 
      data: { 
        textClass: "class2", 
        titleClass: "class1" 
      } 
    }
</script>
```

The previous example renders the _Description_ text in blue (applies the first selector combination, and it is overridden by the second selector combination).

## CSS Preprocessing

Quick apps support the CSS preprocessing using [LESS](https://lesscss.org/) and [Saas](https://sass-lang.com/). 

You can use LESS and SAAS features in the stylesheets adding the `lang` attribute to the `<style>` element.

### LESS CSS Preprocessor

LESS is included as a dependency for the development of quick apps. If you want to install the dependency by yourself, just type:

``` bash
npm i less less-loader
```

In order to use LESS in your stylesheets, you must specify the type of syntax (`lang="less"`) on the `<style>` element in the `.ux` document. For instance: 

``` html
<template>
  <div>
    <text id="title">Using LESS</text>
  </div>
</template>
<style lang="less">
  /* import of LESS stylesheets */
  @import './style.less';
  /* LESS rules */
  @red: #842210; 
  .title { 
    color: @red;
  }   
</style>
```

### Saas CSS Preprocessor

Saas is included as a dependency for the development of quick apps. If you want to install the dependency by yourself, just type:

``` bash
npm i node-sass sass-loader
```

In order to use Saas in your stylesheets, you must specify the type of syntax (`lang="scss"`) on the `<style>` element in the `.ux` document. For instance: 

``` html
<template>
  <div>
    <text id="title">Using Saas!!!</text>
  </div>
</template>
<style lang="saas">
  /* import of Saas stylesheets */
  @import './style.scss';
  /* Saas rules */
  $red: #842210; 
  .title { 
    color: @red;
  }   
</style>
```