# Scripting 

[UX documents](./ux-documents) include a specific `<script>` section to define the business logic, data and managing the lifecycle events of components, pages and apps. 

Scripts on the quick app framework support the standard [ES2015 (ES6)](https://262.ecma-international.org/6.0/) JavaScript syntax. Components and pages defines variables and methods within the `.ux` documents, exporting them to be able to reuse from other components and pages.  

``` html
<template> 
  <div> 
    <text onclick="changeText">{{myDescription}}</text> 
  </div> 
</template>
<script> 
    export default { 
      private: { 
        myDescription: "Hello!"
      },
      changeText(e) {
        // After clicking on "Hello!", text will be changed
        this.myDescription = "Bye!"
      }
    } 
</script>
```

## Import and Export Modules

Quick apps [components](../components/) and [services](../services/) are based on JavaScript modules. We can distribute modules and their members in different files using export and import declarations (`export` and `import` keywords).

You can import specific functions from modules using `import`, following the syntax:

```js
import the_member_to_import from "file_path"
```

For instance, using local scripting files:

``` js
import utils from '../Common/utils.js'
```

Also, you can import scoped packages to use the built-in [services and APIs](../services/). For instance:

``` js
import fetch from "@system.fetch"
```

If you can import a full module:

``` js
import system from "@system"
```

## Data Properties

::: 
The quick app framework uses the `$` prefix when exposing its own built-in data and APIs via the component instances. It also reserves the prefix `_` for internal properties. This is why you should avoid using names for top-level data properties that start with either of these characters.
:::


### Component Instance Properties

Quick apps may access the following variables from any page or component of the app.  

| Property | Type | Description | 
| -------- | ---- | ----------- |
| `$app` | `object` | Object with information about the app (see its [properties](#app)). |
| `$page` | `object` | Object with information about the current page (see its [properties](#page)). |
| `$valid` | `boolean` | Flag that indicates whether a component is valid. |
| `$visible` | `boolean` | Flag that indicates whether a page is visible to users or not. |


#### `$app`

| Property | Type | Description | 
| -------- | ---- | ----------- |
| `$def` | `object` | Access to the objects exposed in the `app.ux` file (you can access it from any component using `this.$app.$def`). |
| `$data` | `object` | Access to global data specified in the [`config.data`](./manifest#data) section of the [manifest](./manifest). (you can access it from any component using `this.$app.$data`) |


#### `$page`

You can access other properties related to the current page,

For instance:

``` js
  console.log(this.$page) 
```

Produces this output on the console:
``` js
{
    "uri":"hap://app/org.example.myquickapp/Hello",
    "windowWidth":1080,
    "windowHeight":2211,
    "statusBarHeight":129,
    "name":"Hello",
    "component":"hello",
    "path":"/Hello",
    "orientation":"portrait",
    "query":{}
}
```

### Page Properties

Quick app pages declare the following properties:

| Property | Type | Description | 
| ------ | ---- | ----------- |
| `private` | `object` | Private data model of a page. This object contains variables that are only accessible under the scope of the current page. |
| `protected` | `object` | Protected data model of a page. This object contains variables that only can be modified by parameters distributed during the page redirection within the current app. |
| `public` | `object` | Public data model of a page. This object can be modified from an external page, using URL parameters. |
| `props` |	`array` | Public properties of a page. This variable is a list of public arguments of the page that is visible externally. Values for these properties can be passed to a page instance using `<component my-argument='value'>`. Names of the properties cannot start with a dollar sign (`$`) or an underscore (`_`) character, and must be in lowercase. Names cannot contain the reserved terms `for`, `if`, `show`, and `tid`. These public properties cannot be functions.
| `computed` | `object` | Computed properties for reactivity and complex calculations. Names of the object members cannot start with a dollar sign (`$`) or an underscore (`_`)character, or contain the reserved terms `for`, `if`, `show`, and `tid`. |
| `data` (deprecated) | `object` or `function` | Data of a page which can be converted into a JSON object. `data` attribute names cannot start with a dollar sign (`$`) or an underscore (`_`) character, and cannot be the same of the element attributes `for`, `if`, `show`, or `tid`. If `data` is a function it must return an `object` and the data variable will be set to the function return value during the page initialization. `data` is deprecated. You should use the attribute `public` instead. The `data` attribute cannot coexist with the `private`, `protected`, and `public` attributes. | 


### Custom Components Properties

You can use the following properties on the instances of custom components.

| Member | Type | Description | 
| ------ | ---- | ----------- |
| `data` | `object` or `function` | Data model of the custom component, which can be converted into a JSON object. Names of the object members cannot start with a dollar sign (`$`) or an underscore (`_`), or contain the reserved terms `for`, `if`, `show`, and `tid`. If the type of `data` is a function, its return value must be an `object` and the value of `data` will be set to the returned value during the page initialization. |
| `props` | `array` or `object`	| Public argument list used to define the component (visible externally). You can pass the properties when declaring the component using attributes on the element (e.g., `<component property='value'>`). Names of the properties cannot start with a dollar sign (`$`) or an underscore (`_`) character, and must be in lowercase. Names cannot contain the reserved terms `for`, `if`, `show`, and `tid`. Properties cannot be functions. |
| `computed` | `object` | Computed properties for reactivity and complex calculations. Names of the object members cannot start with a dollar sign (`$`) or an underscore (`_`)character, or contain the reserved terms `for`, `if`, `show`, and `tid`. |

Example of `props` definition:

``` js
    props: { 
        // a must be a number. 
        a: Number, 
        // You can also do verifications 
        b: { 
            type: Number,       // Check the type 
            default: 0,         // Add default value 
            required: true,     // This is a mandatory attribute 
            validator: function (value) {  // A complex validation 
                // The value must match one of the following character strings: 
                return ['success', 'warning', 'danger'].indexOf(value) !== -1 
            } 
        }  
    }
```

### Computed Properties 

Computed properties let you declare complex logic in functions that includes reactive data.

Methods and properties have the same end result, but computed properties are cached based on their reactive dependencies. A computed property will only re-evaluate when some of its reactive dependencies have changed.

Computed properties are by default getter-only, but you can also provide a setter when you need it:

``` html
<script>
  export default {
    private: { 
      firstName: 'Quick', 
      lastName: 'App' 
    },       
    computed: {
      fullName: {
        // getter
        get() {
          return `${this.firstName} ${this.lastName}`
        },
        // setter
        set(newValue) {
          const names = newValue.split(' ')
          this.firstName = names[0]
          this.lastName = names[names.length - 1]
        }
      }
    },
    onReady () { 
        console.log(this.fullName)  // Quick App 
        this.fullName = 'John Doe' 
        console.log(this.firstName) // John 
        console.log(this.lastName)  // Doe 
    }, 
  }
</script>
```

## Built-in Methods

### Data Properties Management

| Method | Type | Parameter(s) | Description |
| ------ | ---- | ------------ | ----------- |
| `$set` | `function` | `key (string)`, `value (any)` | Adds a data property. You must use this method in the `onInit` function (e.g., `this.$set('key',value)`). |
| `$delete` | `function` | `key (string)` | Removes a data property with the `key` specified. You must use this method in the `onInit` function (e.g., `this.$delete('key')`). |

### Public Methods

| Method | Type | Parameter(s) | Description |
| ------ | ---- | ------------ | ----------- |
| `$element` | `function` | `id (string)` | This method gets the DOM object of the element with the specified ID. If no ID is specified, the method returns the DOM object of the root element of the component. |
| `$root` | `function` | N/A | This method gets the root component instance of the current component. If the current component instance has no parents this value will be itself. |
| `$parent` | `function` | N/A | This method gets the parent ViewModel instance, if the current component instance has one. |
| `$child` | `function` | `id (string)` | This method gets the ViewModel object of a child custom component identified with that ID. |
| `$rootElement` (Deprecated) | `function` | N/A | This method gets the DOM object of the root element. This method is deprecated, use  `this.$element()` instead. |
| `$vm` (deprecated) | `function` | `id (string)` | This method gets the ViewModel of a custom element with the specified ID. This method is deprecated, use `this.$element(id)` instead. |
| `$forceUpdate` | `function` | N/A | This method forces the component instance to re-render. Note it does not affect all child components, only the instance itself and child components with inserted slot content. |
| `getApp` | `function` | N/A | This method gets information about the app (information in the `$app` property). |

### Component Instance Methods

| Method | Type | Parameter(s) | Description |
| ------ | ---- | ------------ | ----------- |
| `$on` | `function` | `event-name (string)` , `handler (function)` | This method binds an event `handler` to an `event-name` event on the current element (e.g., `this.$on('click', this.clickHandler)`, where `clickHandler` is a method defined within the component scope. |
| `$off` | `function` | `event-name (string)` , `handler (function)` | This method removes the listener on an event handler (e.g., `this.$off('click')`). |
| `$dispatch` | `function` | `event-name (string)` | This method propagates event notifications to the parent element (e.g., `this.$dispatch('click')`. Event notifications are sent to the parent element until `event.stop()` is called in the event handler. |
| `$broadcast` | `function` | `event-name (string)` | This method propagates event notifications to the children elements (e.g.,`this.$broadcast('click')`. Event notifications are sent to the children nodes until `event.stop()` is called in the event handler. |
| `$emit` | `function` | `event-name (string)`, `data (object)` | This method triggers an event with the parameters specified (e.g., `this.$emit('click')`, `this.$emit('click', {identifier: 1})`. The parameters can be accessed through `event.detail` in the event handler method (e.g., `event.detail.identifier`). |
| `$emitElement` | `function` | `event-name (string)`, `data (object)`, `id (string)` | Triggers an event on the specified element (identified by the `id` parameter, or `-1` by default that indicates the root element). The parameters passed as parameters may be accessed through `event.detail` in the handler method (e.g., `event.detail.identifier`). |
| `$watch` | `function` | `property-name (string)`  | This method watches a reactive property or a computed function on the component instance for changes. The callback gets called with the new value and the old value for the given property. We can only pass top-level data, `props`, or computed property name as a string (e.g., `this.$watch('a','handler')`). |


### `$app` Methods 

The following method can be accessed through the `$app` property.

| Method | Type | Parameter(s) | Description |
| ------ | ---- | ------------ | ----------- |
| `exit` | `function` | N/A | This method closes the quick app and ends the app lifecycle. The system triggers the `destroy` events on the page component and on the app. |

### `$page` Methods 

The following method can be accessed through the `$page` property.

| Method | Type | Parameter(s) | Description |
| ------ | ---- | ------------ | ----------- |
| `setTitleBar` | `function` | `options ({ text: string, textColor: string, backgroundColor: string, backgroundOpacity: number, menu: boolean }` | This methods configures the title bar of the current page with the optional attributes of the options (e.g., `this.$page.setTitleBar({text:'Hello', textColor:'#FF0000', backgroundColor:'#FFFFFF',backgroundOpacity :0.5,menu: false}`). |
| `name` | `function` | - | This method gets the name of the current page from the manifest (e.g., `this.$page.name`). |
| `component` | `function` | - | This method gets the name of the current page component from the manifest  (e.g.,`this.$page.component`). |
| `path` | `function` | - | This method gets the path of the current page from the manifest (e.g., `this.$page.path`). |
| `finish` | `function` | - | Ends the execution of current page and terminates the page lifecycle, calling the `destroy` event of the page. |
| `orientation` | `function` | - | This method gets the orientation of the current page defined in the manifest (e.g. `this.$page.orientation`). |
| `statusBarHeight` | `function` | - | This method gets the height of the status bar (e.g., `this.$page.statusBarHeight`). |
| `windowHeight` | `function` | - | This method gets the viewport height of the current page (e.g., `this.$page.windowHeight`). |
| `windowWidth` | `function` | - | This method gets  the viewport width of the current page (e.g., `this.$page.windowWidth`). |
| `setStatusBar` | `function` | `options ({ immersive: boolean, textStyle: string, backgroundColor: string, backgroundOpacity: number })` | This methods configures the status bar of the current page with the optional attributes of the options (e.g., `this.$page.setStatusBar({immersive:true, textStyle:'dark', backgroundColor:'#FFFFFF', backgroundOpacity:0.5})`). |
| `exitFullscreen` | `function` | - | Exits the full-screen mode. |
| `query` | `function` | - | This method gets an object with the querystring parameters passed by the router after opening a page (e.g., `this.$page.query`). |
| `scrollBy` | `function` | `options ({ top: number, behavior: string})` | This method scrolls a page by the specified vertical offset (e.g., `this.$page.scrollBy({top:500, behavior:"smooth"})`). This method takes effect only when the page is displayed (after `show` event). |
| `scrollTo` | `function` | `options ({ top: number, behavior: string})` | This method scrolls to a specified position on the page (e.g., `this.$page.scrollTo({top:0, behavior:"smooth"})`).|
| `setSecure` | `function` | `enable-secure (boolean)` | This method sets a flag to configure the system to prevent screen captures (e.g., `this.$page.setSecure(true/false)`). |


