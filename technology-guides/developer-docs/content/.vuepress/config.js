const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Quick Apps',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: 'Quick App reference guide for developers',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#38b5ff' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  base: '/developers/',

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    repoLabel: '',
    docsRepo: 'https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative',
    docsDir: 'technology-guides/developer-docs/content',
    docsBranch: 'master',
    editLinks: true,
    editLinkText: 'Help us improve this document!',
    searchPlaceholder: 'Search...',
    lastUpdated: true,
    nav: [
      {
        text: 'Guide',
        link: '/guide/'
      },
      {
        text: 'UI Components',
        link: '/components/'
      },
      {
        text: 'API & Services',
        link: '/services/'
      },
      {
        text: 'Quick App Initiative',
        link: 'https://quick-app-initiative.ow2.io'
      },
    ],
    sidebar: [
      {
        title: 'Guide',
        collapsable: true,
        children: [
          '/guide/',
          '/guide/getting-started',
          '/guide/directory',
          '/guide/manifest',
          '/guide/ux-documents',
          '/guide/templates',
          '/guide/styling',
          '/guide/scripting',
          '/guide/events',
          '/guide/lifecycle',
          '/guide/i18n',
          '/guide/components-basics',
          '/guide/api-basics',
          '/guide/background',
          '/guide/distribution',
        ]
      },
      {
        title: 'UI Components',
        collapsable: true,
        children: [
          '/components/',
          '/components/attributes',
          '/components/styles',
          '/components/events',
          { 
            title: 'Basic Elements',
            children: [
              '/components/a',
              '/components/image',
              '/components/popup',
              '/components/progress',
              '/components/rating',
              '/components/slot',              
              '/components/span',
              '/components/text',
              '/components/web',
            ],
          },
          { 
            title: 'Containers',
            children: [
              '/components/div',
              '/components/list',
              '/components/list-item',
              '/components/refresh',
              '/components/richtext',
              '/components/stack',
              '/components/swiper',          
              '/components/tab-bar',
              '/components/tab-content',
              '/components/tabs',              
            ],
          },
          { 
            title: 'Forms',
            children: [
              '/components/input',
              '/components/label',
              '/components/option',
              '/components/picker',
              '/components/select',
              '/components/slider',
              '/components/switch',
              '/components/textarea',
            ],
          },
          { 
            title: 'Media',
            children: [
              '/components/animation',              
              '/components/camera',
              '/components/canvas',
              '/components/marquee',
              '/components/transform',
              '/components/video',
            ],
          },
        ]
      },
      {
        title: 'API & Services',
        collapsable: true,
        children: [
          '/services/',
          {
            title: 'Basic Services',
            children: [
              '/services/app-context.md',
              '/services/background-running.md',
              '/services/logging.md',
              '/services/package-management.md',
              '/services/page-routing.md',
              '/services/webview.md',                              
            ]
          },          
          {
            title: 'UI Interaction',
            children: [
              '/services/notification.md',
              '/services/pop-up.md',
              '/services/qr-code.md',
              '/services/sharing.md',
              '/services/text-to-speech.md',
              '/services/vibration.md',
            ]
          },
          {
            title: 'Network',
            children: [
              '/services/fetch.md',
              '/services/download.md',
              '/services/network-status.md',              
              '/services/request.md',
              '/services/upload.md',
              '/services/upload-download.md',
              '/services/websocket.md',
            ]
          },
          {
            title: 'Files and Data Management',
            children: [
              '/services/crypto.md',               
              '/services/data-exchange.md',
              '/services/data-storage.md',
              '/services/file-storage.md',
              '/services/zip.md',
            ]
          },
          {
            title: 'System Services',
            children: [
              '/services/alarms.md',
              '/services/app-configuration.md',
              '/services/audio-volume.md',              
              '/services/battery-level.md',
              '/services/bluetooth.md',
              '/services/calendar.md',
              '/services/clipboard.md',
              '/services/contacts.md',
              '/services/device.md',
              '/services/home-screen-icon.md',
              '/services/location.md',
              '/services/mediaquery.md',
              '/services/screen-brightness.md',
              '/services/sensor.md',
              '/services/sms.md',
              '/services/wifi.md',
            ]
          },
          {
            title: 'Multimedia',
            children: [
              '/services/animations.md',
              '/services/audio.md',
              '/services/recording.md',
              '/services/canvas.md',
              '/services/image-processing.md',
              '/services/multimedia.md',
              '/services/video.md',
            ]
          },         
        ]
      }
    ],
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    '@vuepress/active-header-links',
    [
      "vuepress-plugin-matomo",
      {
        'siteId': 17,
        'trackerUrl': "https://matomo.ow2.org/"
      },
    ]    
  ]
}
