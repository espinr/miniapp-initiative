# Quick App White Paper

You can access/download the white paper in the following formats:

- [HTML](https://quick-app-initiative.ow2.io/page/whitepaper/ "Quick App White Paper in HTML")
- [PDF](./Quick_Apps_White_Paper.pdf "Quick App White Paper in PDF")
- [DOCX](./Quick_Apps_White_Paper.docx "Quick App White Paper in DOCX")

The white paper is licensed under an Attribution 4.0 International ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)) License

### Feedback? 

[Raise an issue](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) with your comment or question. 


